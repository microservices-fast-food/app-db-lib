package ai.ecma.appdblib.entity.enums;

public enum ReceiverTypeEnum {
    APP(""),
    BOT(""),
    ALL("");

    private final String nameUz;

    public String getNameUz() {
        return nameUz;
    }

    ReceiverTypeEnum(String nameUz) {
        this.nameUz = nameUz;
    }
}
