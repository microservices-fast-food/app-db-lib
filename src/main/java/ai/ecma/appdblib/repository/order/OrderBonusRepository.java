package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.bonus.OrderBonus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface OrderBonusRepository extends JpaRepository<OrderBonus, UUID> {

    Optional<OrderBonus> findByOrderId(UUID orderId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE order_bonus\n" +
            "SET box_name=:boxName\n" +
            "    where order_id=:orderId", nativeQuery = true)
    void updateBoxName(@Param("boxName") String boxName,
                       @Param("orderId") UUID orderId);

    @Query(value = "select CAST(op.product_price_bonus_id as varchar ) from order_bonus op\n" +
            "where order_id=:orderId", nativeQuery = true)
    UUID getProductPriceBonusId(@Param("orderId") UUID orderId);

}
