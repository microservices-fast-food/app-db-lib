package ai.ecma.appdblib.entity.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ai.ecma.appdblib.entity.enums.PermissionEnum.*;

public enum RoleEnum {
    COURIER(new ArrayList<>(Arrays.asList(
            CHECK,
            CHANGE_STATUS_USER
    ))),
    USER(new ArrayList<>(Arrays.asList(
            CHECK
    ))),
    ADMIN(new ArrayList<>(Arrays.asList(
            PermissionEnum.values()
    ))),
    PAYME((new ArrayList<>(Arrays.asList(
            CHECK,
            DO_PAYMENT
    )))),
    OPERATOR(new ArrayList<>(Arrays.asList(
            CHECK,
            VIEW_COMMENT,
            READ_COMMENT,
            VIEW_BONUS,
            VIEW_ORDER,
            ACCEPT_OR_REJECT_ORDER
    ))),
    DISPATCHER(new ArrayList<>(Arrays.asList(
            CHECK,
            VIEW_BRANCH_USER,
            VIEW_USER,
            CHANGE_STATUS_USER,
            VIEW_COMMENT,
            READ_COMMENT,
            VIEW_BONUS,
            VIEW_ORDER,
            ATTACH_COURIER_ORDER
    ))),
    KITCHEN(new ArrayList<>(Arrays.asList(
            CHECK,
            ACCEPT_OR_REJECT_ORDER
    )));

    private List<PermissionEnum> permissionEnums;

    RoleEnum(List<PermissionEnum> permissionEnums) {
        this.permissionEnums = permissionEnums;
    }

    public List<PermissionEnum> getPermissionEnums() {
        return permissionEnums;
    }

}
