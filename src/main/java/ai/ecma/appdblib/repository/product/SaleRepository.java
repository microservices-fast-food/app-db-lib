package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface SaleRepository extends JpaRepository<Sale, UUID> {


    @Query(value = "select * from sale\n" +
            "order by created_at desc ",nativeQuery = true)
    List<Sale> getAllByCreatedAtOrderByDesc();
    
}
