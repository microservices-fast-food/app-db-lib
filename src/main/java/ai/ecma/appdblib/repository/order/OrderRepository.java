package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.appdblib.entity.enums.OrderTypeEnum;
import ai.ecma.appdblib.entity.order.Order;
import ai.ecma.appdblib.utils.SqlUniqueIndexes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;

public interface OrderRepository extends JpaRepository<Order, UUID> {
    @Transactional
    @Modifying
    @Query(value = SqlUniqueIndexes.ORDER_UNIQUE_INDEX, nativeQuery = true)
    void uniqueCreator();

    @Transactional
    @Modifying
    @Query(value = "UPDATE orders\n" +
            "set status=:status\n" +
            "where id IN :orderIds", nativeQuery = true)
    void updateOrderStatusByIds(@Param("status") OrderStatusEnum status,
                                @Param("orderIds") Set<UUID> orderIds);

    Optional<Order> findByCustomerIdAndStatus(UUID customerId, OrderStatusEnum status);

    Page<Order> findAllByStatus(OrderStatusEnum status, Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "UPDATE orders\n" +
            "SET price=(select sum(op.price) from order_product op\n" +
            "    where op.order_id=orders.id)\n" +
            "where orders.status=:status", nativeQuery = true)
    void updatePriceOrder(@Param("status") OrderStatusEnum status);

    @Query(value = "select code\n" +
            "from orders\n" +
            "ORDER BY created_at DESC limit 1;", nativeQuery = true)
    Long getOrderCodeByCreatedAt();

    List<Order> findAllByStatusAndIdIn(OrderStatusEnum status, Collection<UUID> id);


    @Transactional
    @Modifying
    @Query(value = "update orders\n" +
            "set status = :newStatus\n" +
            "where plan_received_time <= :futureTime  and plan_received_time >= :currentTime" +
            " and status = :oldStatus and deleted=false\n"
            ,nativeQuery = true)
    void changeOrdersStatusForKitchen(@Param("oldStatus") String oldStatus,
                                      @Param("newStatus") String newStatus,
                                      @Param("futureTime") Timestamp futureTime,
                                      @Param("currentTime") Timestamp currentTime);




    @Query(value = "select *\n" +
            "from order\n" +
            "where :planReceivedTime >= plan_received_time and :currentTime<=plan_received_time" +
            "       and deleted=false and active and branch_id=:branchId\n"
            ,nativeQuery = true)
    List<Order>getOrdersForKitchen(@Param("branchId") UUID branchId,
                                   @Param("planReceivedTime") Timestamp planReceivedTime,
                                   @Param("currentTime") Timestamp currentTime);


    List<Order> findAllByCourierIdNullAndBranchIdAndStatusAndType(UUID branchId, OrderStatusEnum status, OrderTypeEnum type);

    Page<Order> findAllByCustomerIdAndStatusNot(UUID customerId, OrderStatusEnum status, Pageable pageable);


    List<Order> findAllByStatusAndBranchIdOrderByCreatedAt(OrderStatusEnum kitchen, UUID branchId);
}
