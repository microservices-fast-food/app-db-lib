package ai.ecma.appdblib.entity.order.bonus;

import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update box_product set deleted=true where id=?")
@Where(clause = "deleted=false")
public class BoxProduct extends AbsEntity {

    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "product_id")
    private UUID productId;

    @Column(nullable = false)
    private String boxName;

    @JoinColumn(name = "product_price_bonus_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductPriceBonus productPriceBonus;

    @Column(name = "product_price_bonus_id")
    private UUID productPriceBonusId;

    public BoxProduct(UUID productId, String boxName, UUID productPriceBonusId) {
        this.productId = productId;
        this.boxName = boxName;
        this.productPriceBonusId = productPriceBonusId;
    }
}
