package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.CourierOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CourierOrderRepository extends JpaRepository<CourierOrder, UUID> {

    Optional<CourierOrder> findByCloseFalseAndCourierId(UUID courierId);

    //Eng oxirgi yaratilgan CourierOrder ni olib beradi
    @Query(value = "select * from courier_order\n" +
            "where close and deleted=false and courier_id=:courierId order by created_at desc limit 1", nativeQuery = true)
    Optional<CourierOrder> findAllByCloseAndCreatedAtDesc(@Param("courierId") UUID courierId);

    Optional<CourierOrder> findByCourierIdAndId(UUID courierId, UUID id);

    //Hamma yaratilgan CourierOrder larni listini qaytaradi
    @Query(value = "select * from courier_order\n" +
            "where close and deleted=false and courier_id=:courierId order by created_at desc", nativeQuery = true)
    List<CourierOrder> findAllByCloseOrderByCreatedAt(@Param("courierId") UUID courierId);

    //Id va courierId orqali CourierOrderni olish
    Optional<CourierOrder> findAllByIdAndCourierId(UUID id, UUID courierId);
}
