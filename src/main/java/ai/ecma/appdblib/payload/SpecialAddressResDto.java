package ai.ecma.appdblib.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpecialAddressResDto {

    private UUID Id;

    private Double lat;

    private Double lon;

    private String fullAddress;

    private UUID districtId;

    private String name;
}
