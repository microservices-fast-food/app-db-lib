package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    
}
