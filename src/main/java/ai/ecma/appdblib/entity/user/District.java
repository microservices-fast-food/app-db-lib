package ai.ecma.appdblib.entity.user;

import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update district set deleted=true where id=?")
@Where(clause = "deleted=false")
public class District extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @JoinColumn(insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;

    @Column(name = "region_id",nullable = false)
    private UUID regionId;

    public District(String name, UUID regionId) {
        this.name = name;
        this.regionId = regionId;
    }
}
