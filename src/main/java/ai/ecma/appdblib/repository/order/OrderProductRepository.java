package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.appdblib.entity.order.OrderProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.*;

public interface OrderProductRepository extends JpaRepository<OrderProduct, UUID> {

    Optional<OrderProduct> findByOrderIdAndProductId(UUID order_id, UUID productId);

    @Query(value = "select sum(price) from order_product where order_id=:orderId;", nativeQuery = true)
    Double sumByOrderId(@Param("orderId") UUID orderId);

    @Transactional
    @Modifying
    void deleteAllByOrderId(UUID orderId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE order_product\n" +
            "SET price=:price\n" +
            "where id in (select op.id from order_product op join orders o on o.id = op.order_id\n" +
            "             where o.status=:status\n" +
            "               and op.product_id=:productId)", nativeQuery = true)
    void updatePriceOrderProduct(@Param("price") Double price,
                                 @Param("status") OrderStatusEnum status,
                                 @Param("productId") UUID productId);


    @Query(value = "select CAST(product_id as varchar)  from order_product\n" +
            "where order_id=: orderId", nativeQuery = true)
    Set<String> getProductIdsByOrderId(@Param("orderId") UUID orderId);

    @Query(value = "select CAST(op.product_id as varchar ) , op.count from order_product op\n" +
            "where order_id=: orderId",nativeQuery = true)
    HashMap<String, Integer> getProductIdAndCount(@Param("orderId") UUID orderId);

    @Query(value = "select cast(op.product_id as varchar)\n" +
            "from order_product op\n" +
            "         join orders o on op.order_id = o.id\n" +
            "where o.id in :orderIds and o.deleted=false and op.deleted=false",nativeQuery = true)
    List<String>getOrderProductsByOrderIdIn(@Param("orderIds") List<UUID> orderIds);



    List<String>getOrderProductIdsByOrderIdIn(@Param("orderIds") List<UUID> orderIds);

    List<OrderProduct>findAllByOrderIdIn(List<UUID> orderId);
}
