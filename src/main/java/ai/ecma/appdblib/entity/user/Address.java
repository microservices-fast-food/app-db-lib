package ai.ecma.appdblib.entity.user;

import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update address set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Address extends AbsEntity {

    private Double lat;

    private Double lon;

    private String fullAddress;

    /**
     * JoinColumn - DB dagi boglanishdagi nomi.
     * Insertable - Districtdagi obyectni qo'shib bo'lmaydi, O'rniga Id yoziladi
     */
    @JoinColumn(name = "district_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private District district;

    @Column(name = "district_id")
    private UUID districtId;

    public Address(Double lat, Double lon, String fullAddress, UUID districtId) {
        this.lat = lat;
        this.lon = lon;
        this.fullAddress = fullAddress;
        this.districtId = districtId;
    }

}
