package ai.ecma.appdblib.entity.enums;

import lombok.Getter;

@Getter
public enum OrderTypeEnum {
    DELIVERY(""),
    TAKE_AWAY("");

    private final String nameUz;

    OrderTypeEnum(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUz() {
        return nameUz;
    }
}
