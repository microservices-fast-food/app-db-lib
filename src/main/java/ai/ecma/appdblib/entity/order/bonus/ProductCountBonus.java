package ai.ecma.appdblib.entity.order.bonus;

import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.entity.template.Bonus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update product_count_bonus set deleted=true where id=?")
@Where(clause = "deleted=false")
public class ProductCountBonus extends Bonus {

    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "product_id")
    private UUID productId;

    @Column(nullable = false)
    private Integer productCount;

    @JoinColumn(name = "gift_product_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Product giftProduct;

    @Column(name = "gift_product_id")
    private UUID giftProductId;

    @Column(nullable = false)
    private Integer giftProductCount;
}
