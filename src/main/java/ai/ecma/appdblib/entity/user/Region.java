package ai.ecma.appdblib.entity.user;

import ai.ecma.appdblib.entity.enums.RegionEnum;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update region set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Region extends AbsEntity {

    @Enumerated(EnumType.STRING)
    private RegionEnum name;
}
