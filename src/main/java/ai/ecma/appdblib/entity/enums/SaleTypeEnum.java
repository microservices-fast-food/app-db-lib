package ai.ecma.appdblib.entity.enums;

public enum SaleTypeEnum {
    PERCENT(""),
    MONEY("");

    private final String nameUz;

    SaleTypeEnum(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUz() {
        return nameUz;
    }
}
