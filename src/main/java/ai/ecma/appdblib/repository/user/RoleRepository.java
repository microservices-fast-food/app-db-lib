package ai.ecma.appdblib.repository.user;

import ai.ecma.appdblib.entity.user.Role;
import ai.ecma.appdblib.utils.SqlUniqueIndexes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Optional<Role> findByName(String name);

    boolean existsByName(String name);

    boolean existsByIdNotAndName(UUID id, String name);

    @Modifying
    @Transactional
    @Query(value ="delete from role_permissions where role_id=:roleId" ,nativeQuery = true)
    void deleteOldPermissions(@Param("roleId") UUID roleId);

    @Transactional
    @Modifying
    @Query(value = SqlUniqueIndexes.ROLE_UNIQUE_INDEX, nativeQuery = true)
    void uniqueCreator();




    
}
