package ai.ecma.appdblib.entity.order.bonus;

import ai.ecma.appdblib.entity.enums.BonusTypeEnum;
import ai.ecma.appdblib.entity.template.Bonus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Time;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update product_price_bonus set deleted=true where id=?")
@Where(clause = "deleted=false")
public class ProductPriceBonus extends Bonus {

    @Column(nullable = false)
    private Double minPrice;


    public ProductPriceBonus(String name, BonusTypeEnum type, Time startTime, Time endTime, Date startDate, Date endDate, Double minPrice) {
        super(name, type, startTime, endTime, startDate, endDate);
        this.minPrice = minPrice;
    }


}
