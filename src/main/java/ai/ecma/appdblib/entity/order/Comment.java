package ai.ecma.appdblib.entity.order;

import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.appdblib.entity.template.AbsEntity;
import ai.ecma.appdblib.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update comment set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Comment extends AbsEntity {

    @Column(nullable = false)
    private String title;

    @Column(nullable = false, columnDefinition = "text")
    private String text;

    @Column(nullable = false)
    private Boolean complaint;

    @JoinColumn(name = "attachment_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Attachment photo;

    @Column(name = "attachment_id")
    private UUID attachmentId;

    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @Column(name = "order_id")
    private UUID orderId;

    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "user_id")
    private UUID userId;

    private Boolean unread = true;

    public Comment(String title, String text, Boolean complaint, UUID attachmentId, UUID orderId, UUID userId) {
        this.title = title;
        this.text = text;
        this.complaint = complaint;
        this.attachmentId = attachmentId;
        this.orderId = orderId;
        this.userId=userId;
    }

}
