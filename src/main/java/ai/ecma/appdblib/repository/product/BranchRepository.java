package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface BranchRepository extends JpaRepository<Branch, UUID> {
    @Query(value = "select * from branch b join address a on a.id = b.address_id where a.district_id=:districtId", nativeQuery = true)
    List<Branch> getBranchByDistrictId(@Param(value = "districtId") UUID districtId);


    //Berilgan vaqt va hafta kunida aktiv hamda biz bergan mahsulotlari bor branch larni qaytaradi
    @Query(value = "select b.* from branch b join work_day wd on b.id = wd.branch_id join branch_product bp on b.id = bp.branch_id\n" +
            "where b.deleted=false and b.active\n" +
            "  and bp.product_id IN :productIds and bp.active and bp.deleted=false\n" +
            "  and wd.weekday=:weekday\n" +
            "  and case\n" +
            "        when wd.start_time > wd.end_time then\n" +
            "                     (:time between (select date :kelishilganSana + wd.start_time) and (select date :kelishilganSanaPlusKun + wd.end_time))\n" +
            "        when wd.start_time < wd.end_time then\n" +
            "                     (:time between (select date :kelishilganSana + wd.start_time) and (select date :kelishilganSana + wd.end_time)) end", nativeQuery = true)
    List<Branch> getBranchByWorkingAndHasProducts(@Param("productIds") Set<UUID> productIds,
                                                  @Param("weekday") String weekday,
                                                  @Param("time")Timestamp time);
    
}
