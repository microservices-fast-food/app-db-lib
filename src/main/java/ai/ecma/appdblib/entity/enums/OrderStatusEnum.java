package ai.ecma.appdblib.entity.enums;

public enum OrderStatusEnum {
    CLOSED("", ""),
    CANCEL("", ""),
    REJECT("", ""),
    RECEIVED("", ""),
    ON_THE_WAY("", ""),
    READY("", ""),
    ACCEPTED("", ""),
    PENDING("", ""),
    DRAFT("", ""),
    KITCHEN("", "");

    private final String nameUz;
    private final String descriptionUz;

    OrderStatusEnum(String nameUz, String descriptionUz) {
        this.nameUz = nameUz;
        this.descriptionUz = descriptionUz;
    }

    public String getNameUz() {
        return nameUz;
    }

    public String getDescriptionUz() {
        return descriptionUz;
    }
}
