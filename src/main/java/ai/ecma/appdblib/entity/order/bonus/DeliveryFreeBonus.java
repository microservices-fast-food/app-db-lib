package ai.ecma.appdblib.entity.order.bonus;

import ai.ecma.appdblib.entity.template.Bonus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update delivery_free_bonus set deleted=true where id=?")
@Where(clause = "deleted=false")
public class DeliveryFreeBonus extends Bonus {

    @Column(nullable = false)
    private Double minPrice;
}
