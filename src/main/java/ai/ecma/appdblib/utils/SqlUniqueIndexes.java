package ai.ecma.appdblib.utils;

public interface SqlUniqueIndexes {
    String PRODUCT_UNIQUE_INDEX="" +
            "DROP INDEX IF EXISTS uk_product_name;\n" +
            "CREATE UNIQUE INDEX uk_product_name " +
            "ON product (name) WHERE (deleted=false);\n" +
            "DROP INDEX IF EXISTS uk_branch_product_product_id_and_branch_id;\n" +
            "CREATE UNIQUE INDEX uk_branch_product_product_id_and_branch_id " +
            "ON branch_product (product_id, branch_id) WHERE (deleted=false); \n"+
            "DROP INDEX IF EXISTS uk_category_name;\n"+
            "CREATE UNIQUE INDEX uk_category_name " +
            "ON category (name) WHERE (deleted=false);";

    String ROLE_UNIQUE_INDEX = "DROP INDEX IF EXISTS uk_role_name;\n"+
            "CREATE UNIQUE INDEX uk_role_name" +
            " ON role (name) WHERE (deleted=false);\n"+
            "DROP INDEX IF EXISTS uk_user_phone_number;\n"+
            "CREATE UNIQUE INDEX uk_user_phone_number" +
            " ON users (phone_number) WHERE (deleted=false);"+
            "DROP INDEX IF EXISTS uk_district_name;\n"+
            "CREATE UNIQUE INDEX uk_district_name" +
            " ON district (name) WHERE (deleted=false);"+
            "DROP INDEX IF EXISTS uk_user_address;\n"+
            "CREATE UNIQUE INDEX uk_user_address" +
            " ON user_address (address_id) WHERE (deleted=false);";

    String ORDER_UNIQUE_INDEX = "DROP INDEX IF EXISTS uk_pay_type_name;\n" +
            "CREATE UNIQUE INDEX uk_pay_type_name" +
            " ON pay_type (name) WHERE (deleted=false);";


}
