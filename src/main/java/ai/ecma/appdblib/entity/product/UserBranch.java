package ai.ecma.appdblib.entity.product;

import ai.ecma.appdblib.entity.template.AbsEntity;
import ai.ecma.appdblib.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update user_branch set deleted=true where id=?")
@Where(clause = "deleted=false")
public class UserBranch extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User courier;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Branch branch;

    @Column(nullable = false)
    private Boolean active = false;
}
