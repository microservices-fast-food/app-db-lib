package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.bonus.ProductCountBonus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductCountBonusRepository extends JpaRepository<ProductCountBonus, UUID> {
    @Query(value = "select *\n" +
            "from product_count_bonus\n" +
            "where ((:startDate <= start_date and :endDate >= start_date) or\n" +
            "       (:startDate <= end_date and :endDate >= end_date) or\n" +
            "       (:startDate >= start_date and :endDate <= end_date) or\n" +
            "       (:startDate <= start_date and :endDate >= end_date)) and deleted=false and active\n" +
            "  and case\n" +
            "          when start_time > end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSanaPlusKun + end_time) between :startTime and :endTime)\n" +
            "\n" +
            "          when start_time < end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSana + end_time) between :startTime and :endTime)\n" +
            "    end;",nativeQuery = true)
    List<ProductCountBonus> getExistsByGivenTime(@Param("startDate") Date startDate,
                                         @Param("endDate")Date endDate,
                                         @Param("startTime") Timestamp startTime,
                                         @Param("endTime") Timestamp endTime,
                                         @Param("kelishilganSana") String kelishilganSana,
                                         @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun);

    @Query(value = "select *\n" +
            "from product_count_bonus\n" +
            "where ((:startDate <= start_date and :endDate >= start_date) or\n" +
            "       (:startDate <= end_date and :endDate >= end_date) or\n" +
            "       (:startDate >= start_date and :endDate <= end_date) or\n" +
            "       (:startDate <= start_date and :endDate >= end_date)) and deleted=false and active and id!=:id\n" +
            "  and case\n" +
            "          when start_time > end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSanaPlusKun + end_time) between :startTime and :endTime)\n" +
            "\n" +
            "          when start_time < end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSana + end_time) between :startTime and :endTime)\n" +
            "    end;",nativeQuery = true)
    List<ProductCountBonus> getExistsByGivenTimeIdNot(@Param("startDate") Date startDate,
                                              @Param("endDate")Date endDate,
                                              @Param("startTime") Timestamp startTime,
                                              @Param("endTime") Timestamp endTime,
                                              @Param("kelishilganSana") String kelishilganSana,
                                              @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun,
                                              @Param("id") UUID id);


    // bu metod berilgan buyurtmadagi product lar va ularning sonini tekshirib,
    // mavjud bonuslardagi product sonidan katta yoki teng bo'lsa va hozirgi vaqt
    // aksiya amal qilish muddati ichida bo'lsa, shu aksiyani qaytaradi
    @Query(value = "with tempProducts as (select op.product_id, op.count from order_product op where order_id =:orderId)\n" +
            "select pcb.*\n" +
            "from product_count_bonus as pcb\n" +
            "join tempProducts on pcb.product_id=tempProducts.product_id\n" +
            "where  tempProducts.count >= pcb.product_count\n" +
            "    and deleted=false\n" +
            "            and (:date >= start_date and :date <= end_date)\n" +
            "              and case\n" +
            "                      when start_time > end_time then\n" +
            "                          (:time between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time))\n" +
            "                      when start_time < end_time then\n" +
            "                          (:time between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)) end;\n",nativeQuery = true)
    Optional<ProductCountBonus> getProductCountBonusByCurrentTime(@Param("orderId") UUID orderId,
                                                                  @Param("date") Date date,
                                                                  @Param("time") Timestamp time,
                                                                  @Param("kelishilganSana") String kelishilganSana,
                                                                  @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun);

    //productCountId bo'yicha giftProduct id va count dan hashMap yasaydi
    @Query(value = "select CAST(gift_product_id as varchar), gift_product_count from product_count_bonus  where id=:id and deleted=false",nativeQuery = true)
    HashMap<String, Integer> getGiftProductIdAndCount(@Param("id") UUID id);
}
