package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.Property;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PropertyRepository extends JpaRepository<Property, UUID> {

    Optional<Property> findByKey(String key);
    
}
