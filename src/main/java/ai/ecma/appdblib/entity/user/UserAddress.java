package ai.ecma.appdblib.entity.user;

import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update user_address set deleted=true where id=?")
@Where(clause = "deleted=false")
public class UserAddress extends AbsEntity {

    private String name;  // uyim , ishxona , xolami uyi

    @JoinColumn(insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "user_id")
    private UUID userId;

    @JoinColumn(insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Address address;

    @Column(name = "address_id")
    private UUID addressId;

    public UserAddress(String name, UUID userId, UUID addressId) {
        this.name = name;
        this.userId = userId;
        this.addressId = addressId;
    }
}
