package ai.ecma.appdblib.repository.user;

import ai.ecma.appdblib.entity.enums.RegionEnum;
import ai.ecma.appdblib.entity.user.Region;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RegionRepository extends JpaRepository<Region, UUID> {
    Optional<Region> findByName(RegionEnum name);

}
