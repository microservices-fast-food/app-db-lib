package ai.ecma.appdblib.entity.user;

import ai.ecma.appdblib.entity.enums.ReceiverTypeEnum;
import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update notification set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Notification extends AbsEntity {

    private String title;

    @Column(columnDefinition = "text")
    private String text;

    @JoinColumn(insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Attachment photo;

    @Column(name = "photo_id")
    private UUID photoId;

    private Timestamp sendTime;

    @Enumerated(EnumType.STRING)
    private ReceiverTypeEnum receiver;

    public Notification(String title, String text, UUID photoId, Timestamp sendTime, ReceiverTypeEnum receiver) {
        this.title = title;
        this.text = text;
        this.photoId = photoId;
        this.sendTime = sendTime;
        this.receiver = receiver;
    }
}
