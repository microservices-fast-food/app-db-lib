package ai.ecma.appdblib.entity.order.bonus;

import ai.ecma.appdblib.entity.product.Sale;
import ai.ecma.appdblib.entity.template.Bonus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update sale_bonus set deleted=true where id=?")
@Where(clause = "deleted=false")
public class SaleBonus extends Bonus {

    @Column(nullable = false)
    private Double minPrice;

    @JoinColumn(name = "sale_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Sale sale;

    @Column(name = "sale_id")
    private UUID saleId;

    public SaleBonus(Double minPrice, UUID saleId) {
        this.minPrice = minPrice;
        this.saleId = saleId;
    }
}
