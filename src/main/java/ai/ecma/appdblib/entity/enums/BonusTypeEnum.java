package ai.ecma.appdblib.entity.enums;

public enum BonusTypeEnum {
    SALE_BONUS(""),
    PRODUCT_PRICE_BONUS(""),
    PRODUCT_COUNT_BONUS(""),
    DELIVERY_FREE_BONUS("");

    private final String nameUz;

    BonusTypeEnum(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUz() {
        return nameUz;
    }
}
