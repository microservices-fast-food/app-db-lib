package ai.ecma.appdblib.repository.user;

import ai.ecma.appdblib.entity.user.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface NotificationRepository extends JpaRepository<Notification, UUID> {

}
