package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.bonus.DeliveryFreeBonus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DeliveryFreeBonusRepository extends JpaRepository<DeliveryFreeBonus, UUID> {

    @Query(value = "select *\n" +
            "from delivery_free_bonus \n" +
            "where ((:startDate <= start_date and :endDate >= start_date) or\n" +
            "       (:startDate <= end_date and :endDate >= end_date) or\n" +
            "       (:startDate >= start_date and :endDate <= end_date) or\n" +
            "       (:startDate <= start_date and :endDate >= end_date)) and deleted=false and active\n" +
            "  and case\n" +
            "          when start_time > end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSanaPlusKun + end_time) between :startTime and :endTime)\n" +
            "\n" +
            "          when start_time < end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSana + end_time) between :startTime and :endTime)\n" +
            "    end",nativeQuery = true)
    List<DeliveryFreeBonus> getExistsByGivenTime(@Param("startDate") Date startDate,
                                         @Param("endDate")Date endDate,
                                         @Param("startTime") Timestamp startTime,
                                         @Param("endTime") Timestamp endTime,
                                                 @Param("kelishilganSana") String kelishilganSana,
                                                 @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun);

    @Query(value = "select *\n" +
            "from delivery_free_bonus \n" +
            "where ((:startDate <= start_date and :endDate >= start_date) or\n" +
            "       (:startDate <= end_date and :endDate >= end_date) or\n" +
            "       (:startDate >= start_date and :endDate <= end_date) or\n" +
            "       (:startDate <= start_date and :endDate >= end_date)) and deleted=false and active and id!=:id\n" +
            "  and case\n" +
            "          when start_time > end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSanaPlusKun + end_time) between :startTime and :endTime)\n" +
            "\n" +
            "          when start_time < end_time then\n" +
            "              (:startTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or :endTime between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)\n" +
            "                  or (select date :kelishilganSana + start_time) between :startTime and :endTime\n" +
            "                  or (select date :kelishilganSana + end_time) between :startTime and :endTime)\n" +
            "    end",nativeQuery = true)
    List<DeliveryFreeBonus> getExistsByGivenTimeNotId(@Param("startDate") Date startDate,
                                                 @Param("endDate")Date endDate,
                                                 @Param("startTime") Timestamp startTime,
                                                 @Param("endTime") Timestamp endTime,
                                                      @Param("kelishilganSana") String kelishilganSana,
                                                      @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun,
                                                      @Param("id") UUID id);


    //Ushbu metod hozirda amalda bo'lgan aksiyani berilayotgan narx bonusdagi
    // narxdan katta yoki teng bo'lsa va shu aksiya hozirgi vaqtda amalda bo'lsa
    // tekshiradi
    @Query(value = "select *\n" +
            "from delivery_free_bonus\n" +
            "where :price>=price and (:date >= start_date and :date <= end_date)" +
            "  and case\n" +
            "          when start_time > end_time then\n" +
            "              (:time between (select date :kelishilganSana + start_time) and (select date :kelishilganSanaPlusKun + end_time))\n" +
            "          when start_time < end_time then\n" +
            "              (:time between (select date :kelishilganSana + start_time) and (select date :kelishilganSana + end_time)) end", nativeQuery = true)
    Optional<DeliveryFreeBonus> getDeliveryFreeBonusByCurrentTime(@Param("price") Double price,
                                                                  @Param("date") Date date,
                                                                  @Param("time") Timestamp time,
                                                                  @Param("kelishilganSana") String kelishilganSana,
                                                                  @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun);
}
