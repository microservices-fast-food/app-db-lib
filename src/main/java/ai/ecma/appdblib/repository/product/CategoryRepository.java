package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface CategoryRepository extends JpaRepository<Category, UUID> {
    boolean existsByName(String name);

    boolean existsByIdNotAndName(UUID id, String name);

    int countByParentCategoryId(UUID parentCategory_id);

    @Query(value = "select * from category where parent_category_id is null\n" +
            "and index >=:indexMin and index<=:indexMax", nativeQuery = true)
    List<Category> getByParentCategoryIsNullBetween(@Param("indexMin") Integer indexMin,
                                                    @Param("indexMax") Integer indexMax);

    @Query(value = "select * from category where parent_category_id=:parentCategoryId\n" +
            "and index >=:indexMin and index<=:indexMax", nativeQuery = true)
    List<Category> getByParentCategoryBetween(@Param("parentCategoryId") UUID parentCategoryId,
                                              @Param("indexMin") Integer indexMin,
                                              @Param("indexMax") Integer indexMax);

}
