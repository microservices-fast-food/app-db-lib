package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.RecommendedProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface RecommendedProductRepository extends JpaRepository<RecommendedProduct, UUID> {
    List<RecommendedProduct> findAllByCategoryId(UUID category_id);
    boolean existsByCategoryIdAndProductId(UUID category_id, UUID product_id);
    
}
