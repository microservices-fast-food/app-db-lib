package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.WorkDay;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface WorkDayRepository extends JpaRepository<WorkDay, UUID> {
    
}
