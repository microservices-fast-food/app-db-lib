package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.bonus.BoxProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface BoxProductRepository extends JpaRepository<BoxProduct, UUID> {
    @Query(value = "select b.box_name from box_product b \n" +
            "            where product_price_bonus_id=:productPriceBonusId \n" +
            "            GROUP BY b.box_name", nativeQuery = true)
    Set<String> getBoxNamesByProductPriceBonus(@Param("productPriceBonusId") UUID productPriceBonusId);

    Integer countByBoxNameAndProductPriceBonusId(String boxName, UUID productPriceBonusId);

    @Query(value = "select b.product_id\n" +
            "from box_product b\n" +
            "where product_id = :productPriceBonusId\n" +
            "  and box_name = :boxName", nativeQuery = true)
    Set<String> getProductIdsByBoxNameAndProductPriceBonusId(@Param("productPriceBonusId") UUID productPriceBonusId,
                                                           @Param("boxName") String boxName);

    List<BoxProduct> findAllByProductPriceBonusId(UUID productPriceBonusId);

    @Query(value = "select b.product_id\n" +
            "from box_product b\n" +
            "where product_price_bonus_id = :productPriceBonusId\n" +
            "  and box_name = :boxName", nativeQuery = true)
    Set<String> getAllProductIdByBoxNameAndProductPriceBonusId(@Param("productPriceBonusId") UUID productPriceBonusId,
                                                             @Param("boxName") String boxNAme);

    void deleteAllByProductPriceBonusId(UUID productPriceBonusId);

    boolean existsByBoxNameAndProductPriceBonusId(String boxName, UUID productPriceBonusId);

    @Query(value = "select product_id " +
            "from box_product " +
            "where box_name=:boxName " +
            "and product_price_bonus_id=:productPriceBonusId " +
            "and deleted=false",nativeQuery = true)
    List<String> getProductIdsByBoxNameAndBonusId(@Param("boxName") String boxName,
                                                  @Param("productPriceBonusId") UUID productPriceBonusId);

    List<BoxProduct>findAllByProductPriceBonusIdAndBoxName(UUID productPriceBonusId, String boxName);
}
