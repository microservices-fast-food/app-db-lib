package ai.ecma.appdblib.entity.enums;

public enum RegionEnum {
    TOSHKENT_SH(""),
    ANDIJON(""),
    BUXORO(""),
    FARGONA(""),
    JIZZAX(""),
    XORAZM(""),
    NAMANGAN(""),
    NAVOIY(""),
    QASHQADARYO(""),
    QORAQALPOGISTON_R(""),
    SAMARQAND(""),
    SIRDARYO(""),
    SURXONDARYO(""),
    TOSHKENT("");

    private final String nameUz;

    RegionEnum(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUz() {
        return nameUz;
    }
}
