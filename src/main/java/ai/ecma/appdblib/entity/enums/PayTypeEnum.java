package ai.ecma.appdblib.entity.enums;

public enum PayTypeEnum {
    CASH(""),
    CARD("");
    private final String nameUz;

    PayTypeEnum(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUz() {
        return nameUz;
    }
}
