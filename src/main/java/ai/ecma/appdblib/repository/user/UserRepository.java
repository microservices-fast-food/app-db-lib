package ai.ecma.appdblib.repository.user;

import ai.ecma.appdblib.entity.enums.CourierStatusEnum;
import ai.ecma.appdblib.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumber(String phoneNumber);

    boolean existsByPhoneNumber(String phoneNumber);

    boolean existsByPhoneNumberAndIdNot(String phoneNumber, UUID id);

    Page<User> findAllByRoleId(UUID role_id, Pageable pageable);



    @Query(value = "select u.* from users u join user_branch ub on u.id = ub.courier_id\n" +
            "where ub.branch_id=:branchId and ub.active and u.deleted=false and ub.deleted=false and u.status=:status", nativeQuery = true)
    List<User> getAllOnlineCourierByBranchId(@Param("branchId") UUID branchId,
                                       @Param("status") CourierStatusEnum status);

    List<User> findAllByIdIn(Set<UUID> id);
}
