package ai.ecma.appdblib.repository.user;

import ai.ecma.appdblib.entity.user.UserAddress;
import ai.ecma.appdblib.payload.SpecialAddressResDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserAddressRepository extends JpaRepository<UserAddress, UUID> {

    List<UserAddress> findAllByUserId(UUID userId);

    Optional<UserAddress> findByUserIdAndAddressId(UUID userId, UUID addressId);

    @Transactional
    @Modifying
    @Query(value = "update user_address set deleted=true where address_id =:addressId", nativeQuery = true)
    void deleteByAddressId(@Param("addressId") UUID addressId);

//
//    @Query(value = "select a.*,ad.name from user_address ad\n" +
//            "join address a on a.id = ad.address_id\n" +
//            "where user_id =:userId and a.deleted=false;",nativeQuery = true)

    @Transactional
    @Modifying
    @Query(value = "select a.lat,a.lon,a.full_address,a.district_id,a.id,ad.name from user_address ad\n" +
            "join address a on a.id = ad.address_id\n" +
            "where user_id =:userId and ad.deleted=false",nativeQuery = true)
    List<SpecialAddressResDto> findAllByAddressByUserId(@Param("userId") UUID userId);



}
