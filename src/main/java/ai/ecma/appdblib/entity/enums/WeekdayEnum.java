package ai.ecma.appdblib.entity.enums;

public enum WeekdayEnum {
    MONDAY(""),
    TUESDAY(""),
    WEDNESDAY(""),
    THURSDAY(""),
    FRIDAY(""),
    SATURDAY(""),
    SUNDAY("");

    private final String nameUz;

    WeekdayEnum(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUz() {
        return nameUz;
    }
}
