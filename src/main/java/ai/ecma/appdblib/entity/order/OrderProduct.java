package ai.ecma.appdblib.entity.order;

import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update order_product set deleted=true where id=?")
@Where(clause = "deleted=false")
@Check(constraints = "count > 0")
public class OrderProduct extends AbsEntity {

    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @Column(name = "order_id")
    private UUID orderId;

    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "product_id")
    private UUID productId;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Integer count;

    public OrderProduct(UUID orderId, UUID productId, Double price, Integer count) {
        this.orderId = orderId;
        this.productId = productId;
        this.price = price;
        this.count = count;
    }
}
