package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.enums.PayTypeEnum;
import ai.ecma.appdblib.entity.order.PayType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PayTypeRepository extends JpaRepository<PayType, UUID> {


    List<PayType> findAllByPayType(PayTypeEnum payType);
}
