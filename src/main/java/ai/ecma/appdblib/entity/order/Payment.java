package ai.ecma.appdblib.entity.order;

import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update payment set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Payment extends AbsEntity {
    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    @OneToOne
    private Order order;

    @Column(name = "order_id")
    private UUID orderId;

    @Column(nullable = false)
    private Double amount;

    @JoinColumn(name = "payType_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PayType payType;

    @Column(name = "payType_id")
    private UUID payTypeId;

    public Payment(UUID orderId, Double amount, UUID payTypeId) {
        this.orderId = orderId;
        this.amount = amount;
        this.payTypeId = payTypeId;
    }
}
