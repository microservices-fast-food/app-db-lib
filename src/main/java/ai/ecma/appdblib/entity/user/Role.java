package ai.ecma.appdblib.entity.user;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update role set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Role extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @ElementCollection                         //table bo'lmagan lekin databasada role id va permissionlarni saqlaydigan table ochib beradi
    @Enumerated(EnumType.STRING)
    private Set<PermissionEnum> permissions;
}
