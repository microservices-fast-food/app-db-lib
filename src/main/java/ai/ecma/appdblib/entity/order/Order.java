package ai.ecma.appdblib.entity.order;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.appdblib.entity.enums.OrderTypeEnum;
import ai.ecma.appdblib.entity.order.bonus.OrderBonus;
import ai.ecma.appdblib.entity.product.Branch;
import ai.ecma.appdblib.entity.template.AbsEntity;
import ai.ecma.appdblib.entity.user.Address;
import ai.ecma.appdblib.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "orders")
@SQLDelete(sql = "update orders set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Order extends AbsEntity {

    @Column(nullable = false)
    private Double price;

    private Double deliveryPrice;

    private Long code;

    @JoinColumn(name = "customer_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User customer;

    @Column(name = "customer_id")
    private UUID customerId;

    @Enumerated(EnumType.STRING)
    private OrderTypeEnum type;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatusEnum status;

    @JoinColumn(name = "branch_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Branch branch;

    @Column(name = "branch_id")
    private UUID branchId;

    @JoinColumn(name = "address_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Address address;

    @Column(name = "address_id")
    private UUID addressId;

    @JoinColumn(name = "courier_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private User courier;

    @Column(name = "courier_id")
    private UUID courierId;

    @JoinColumn(name = "operator_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private User operator;

    @Column(name = "operator_id")
    private UUID operatorId;

    @JoinColumn(name = "dispatcher_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private User dispatcher;

    @Column(name = "dispatcher_id")
    private UUID dispatcherId;

    @JoinColumn(name = "pay_type_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PayType payType;

    @Column(name = "pay_type_id")
    private UUID payTypeId;

    private Double distance;  // km da hisoblimiz  masalan 1.5 km

    private Timestamp planReceivedTime;
    private Integer deliveryTime; // minutda qilamiz qancha vaqtda yetib borishi tahminan  ( masalan 40 minut)
    private Integer deliveryRate; //yetkazib berish  bahosi
    private Integer orderRate; //buyuurtmaga baho taomga

    @OneToOne(mappedBy = "order")
    private OrderBonus orderBonus;

    public Order(UUID customerId, OrderStatusEnum status, Double price) {
        this.price = price;
        this.customerId = customerId;
        this.status = status;
    }
}
