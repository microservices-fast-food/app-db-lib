package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.util.UUID;

public interface FavoriteRepository extends JpaRepository<Favorite, UUID> {
    @Transactional
    @Modifying
    void deleteByProductIdAndUserId(UUID product_id, UUID userId);

    @Transactional
    @Modifying
    void deleteAllByUserId(UUID userId);
    boolean existsByUserIdAndProductId(UUID userId, UUID product_id);
    
}
