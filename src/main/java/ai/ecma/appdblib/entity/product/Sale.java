package ai.ecma.appdblib.entity.product;

import ai.ecma.appdblib.entity.enums.SaleTypeEnum;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update sale set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Sale extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    private SaleTypeEnum type;

    @Column(nullable = false)
    private Boolean active;

    @Column(nullable = false)
    private Double amount;
}
