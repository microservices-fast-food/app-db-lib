package ai.ecma.appdblib.repository.user;

import ai.ecma.appdblib.entity.user.District;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DistrictRepository extends JpaRepository<District, UUID> {

    boolean existsByName(String name);
    boolean existsByNameAndIdNot(String name, UUID id);


    
}
