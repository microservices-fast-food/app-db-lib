package ai.ecma.appdblib.entity.order;

import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update property set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Property extends AbsEntity {   //masalan bot name keyga  va bot tokeni valuega  va boshqa holatlar

    @Column(nullable = false)
    private String key;

    @Column(nullable = false)
    private String value;

}
