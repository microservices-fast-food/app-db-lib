package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.bonus.ProductPriceBonus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductPriceBonusRepository extends JpaRepository<ProductPriceBonus, UUID> {
    @Query(value = "select * from product_price_bonus\n" +
            "            where ((:startDate <= start_date and :endDate >= start_date) or\n" +
            "                   (:startDate <= end_date and :endDate >= end_date) or\n" +
            "                   (:startDate >= start_date and :endDate <= end_date) or\n" +
            "                   (:startDate <= start_date and :endDate >= end_date)) and deleted=false\n" +
            "              and case\n" +
            "                      when start_time > end_time then\n" +
            "                          (:startTime between (select  cast (:kelishilganSana as date) +  start_time) and (select  cast (:kelishilganSanaPlusKun as date) + end_time)\n" +
            "                              or :endTime between (select  cast (:kelishilganSana as date) +  start_time) and (select  cast (:kelishilganSanaPlusKun as date) + end_time)\n" +
            "                              or (select  cast (:kelishilganSana as date) + start_time) between :startTime and :endTime\n" +
            "                              or (select  cast (:kelishilganSanaPlusKun as date) + end_time) between :startTime and :endTime)\n" +
            "\n" +
            "                      when start_time < end_time then\n" +
            "                          (:startTime between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSana as date) + end_time)\n" +
            "                              or :endTime between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSana as date) + end_time)\n" +
            "                              or (select  cast (:kelishilganSana as date) + start_time) between :startTime and :endTime\n" +
            "                              or (select  cast (:kelishilganSana as date) + end_time) between :startTime and :endTime)\n" +
            "                end", nativeQuery = true)
    List<ProductPriceBonus> getExistsByGivenTime(@Param("startDate") Date startDate,
                                                 @Param("endDate") Date endDate,
                                                 @Param("startTime") Timestamp startTime,
                                                 @Param("endTime") Timestamp endTime,
                                                 @Param("kelishilganSana") Date kelishilganSana,
                                                 @Param("kelishilganSanaPlusKun") Date kelishilganSanaPlusKun);


    @Query(value = "select *\n" +
            "from product_price_bonus \n" +
            "where NOT id=:id\n" +
            "and ((:startDate <= start_date and :endDate >= start_date) or\n" +
            "       (:startDate <= end_date and :endDate >= end_date) or\n" +
            "       (:startDate >= start_date and :endDate <= end_date) or\n" +
            "       (:startDate <= start_date and :endDate >= end_date)) and deleted=false\n" +
            "  and case\n" +
            "          when start_time > end_time then\n" +
            "              (:startTime between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSanaPlusKun as date) + end_time)\n" +
            "                  or :endTime between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSanaPlusKun as date) + end_time)\n" +
            "                  or (select  cast (:kelishilganSana as date) + start_time) between :startTime and :endTime\n" +
            "                  or (select  cast (:kelishilganSanaPlusKun as date) + end_time) between :startTime and :endTime)\n" +
            "\n" +
            "          when start_time < end_time then\n" +
            "              (:startTime between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSana as date) + end_time)\n" +
            "                  or :endTime between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSana as date) + end_time)\n" +
            "                  or (select  cast (:kelishilganSana as date) + start_time) between :startTime and :endTime\n" +
            "                  or (select  cast (:kelishilganSana as date) + end_time) between :startTime and :endTime)\n" +
            "    end", nativeQuery = true)
    List<ProductPriceBonus> getExistsByGivenTimeIdNot(@Param("startDate") Date startDate,
                                                      @Param("endDate") Date endDate,
                                                      @Param("startTime") Timestamp startTime,
                                                      @Param("endTime") Timestamp endTime,
                                                      @Param("kelishilganSana") String kelishilganSana,
                                                      @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun,
                                                      @Param("id") UUID id);

    @Query(value = "select *\n" +
            "from product_price_bonus ppb\n" +
            "where ((:startDate >=ppb.start_date and :startDate <=ppb.end_date)\n" +
            "or (:startDate <= ppb.start_date and :endDate >= ppb.end_date)\n" +
            "    or (:endDate >= ppb.start_date and :endDate <= ppb.end_date)\n" +
            "    or (:startDate <= ppb.start_date and :endDate <= ppb.end_date))and deleted=false", nativeQuery = true)
    List<ProductPriceBonus> getProductPriceBonusByStartDateAndEndDate(@Param("startDate") Date startDate,
                                                                      @Param("endDate") Date endDate);


    //hozirda ushbu order price ga va sana va hozirgi vaqtga mos aksiya mavjudligini tekshiradi va bo'lsa shu ProductPriceBonus ni qaytaradi
    @Query(value = "select *\n" +
            "from product_price_bonus\n" +
            "where price<=:price " +
            "and deleted=false " +
            "and (:date >= start_date and :date <= end_date)\n" +
            "  and case\n" +
            "          when start_time > end_time then\n" +
            "              (:time between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSanaPlusKun as date) + end_time))\n" +
            "          when start_time < end_time then\n" +
            "              (:time between (select  cast (:kelishilganSana as date) + start_time) and (select  cast (:kelishilganSana as date) + end_time)) end", nativeQuery = true)
    Optional<ProductPriceBonus> getProductPriceBonusByCurrentTime(
            @Param("price") Double price,
            @Param("date") Date date,
            @Param("time") Timestamp time,
            @Param("kelishilganSana") String kelishilganSana,
            @Param("kelishilganSanaPlusKun") String kelishilganSanaPlusKun);


}
