package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.utils.SqlUniqueIndexes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    boolean existsByName(String name);
    boolean existsByNameAndIdNot(String name, UUID id);

    @Query(value = "select * from product where sale_id is not null",nativeQuery = true)
    Page<Product> findAllBySaleIdNotNull(Pageable pageable);
    Page<Product> findAllByCategoryId(UUID category_id, Pageable pageable);
    Page<Product> findAllByActive(Boolean active, Pageable pageable);

    @Query(value = "select p.* from product p join recommended_product rp on p.id = rp.product_id\n" +
            "where rp.category_id=:categoryId", nativeQuery = true)
    Page<Product> getAllRecProductForCategory(@Param("categoryId") UUID categoryId, Pageable pageable);

    @Query(value = "select p.* from product p join favorite f on p.id = f.product_id\n" +
            "where f.user_id=:userId", nativeQuery = true)
    Page<Product> getAllByFavorite(@Param("userId") UUID userId, Pageable pageable) ;

    List<Product> findAllByIdIsIn(Set<UUID> id);


    @Transactional
    @Modifying
    @Query(value = SqlUniqueIndexes.PRODUCT_UNIQUE_INDEX,nativeQuery = true)
    void uniqueCreator();
    
}
