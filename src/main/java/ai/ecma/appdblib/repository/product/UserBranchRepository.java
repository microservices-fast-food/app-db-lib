package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.UserBranch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserBranchRepository extends JpaRepository<UserBranch, UUID> {

}
