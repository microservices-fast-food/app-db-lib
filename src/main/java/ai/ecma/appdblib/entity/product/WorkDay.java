package ai.ecma.appdblib.entity.product;

import ai.ecma.appdblib.entity.enums.WeekdayEnum;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Time;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update work_day set deleted=true where id=?")
@Where(clause = "deleted=false")
public class WorkDay extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Branch branch;

    @Enumerated(EnumType.STRING)
    private WeekdayEnum weekday;

    @Column(nullable = false)
    private Time startTime;

    @Column(nullable = false)
    private Time endTime;
}
