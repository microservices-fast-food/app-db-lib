package ai.ecma.appdblib.entity.product;

import ai.ecma.appdblib.entity.template.AbsEntity;
import ai.ecma.appdblib.entity.user.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update branch set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Branch extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @JoinColumn(name = "address_id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Address address;

    @Column(name = "address_id")
    private UUID addressId;

    private Boolean active = false;

    @Column(nullable = false)
    private Integer cookingTime;            // qancha vaqtda tayyor bo'lishini daqiqada kiritadi admin

    private Boolean autoDelivery = false;  // buyurtmani avtomatik holatda yetkazib berish admin aralashishisiz

    private Double deliveryMaxKm;

    public Branch(String name, UUID addressId, Boolean active, Integer cookingTime, Boolean autoDelivery, Double deliveryMaxKm) {
        this.name = name;
        this.addressId = addressId;
        this.active = active;
        this.cookingTime = cookingTime;
        this.autoDelivery = autoDelivery;
        this.deliveryMaxKm=deliveryMaxKm;
    }
}
