package ai.ecma.appdblib.entity.user;

import ai.ecma.appdblib.entity.enums.CourierStatusEnum;
import ai.ecma.appdblib.entity.enums.LanguageEnum;
import ai.ecma.appdblib.entity.order.CourierOrder;
import ai.ecma.appdblib.entity.order.PayType;
import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
@SQLDelete(sql = "update users set deleted=true where id=?")
@Where(clause = "deleted=false")
public class User extends AbsEntity {

    @Column(nullable = false)
    private String firstName;

    private String lastName;

    @Column(nullable = false)
    private String phoneNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    private District district;

    @JoinColumn(insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Attachment avatar;

    @Column(name = "avatar_id")
    private UUID avatarId;

    @Column(nullable = false)
    private Date birthdate;

    @Enumerated(EnumType.STRING)
    private LanguageEnum language;

    private CourierStatusEnum status;

    @JoinColumn(insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private PayType paytype;

    @Column(name = "paytype_id")
    private UUID paytypeId;

    private Integer reliabilityRate;

    @ManyToOne( fetch = FetchType.LAZY, optional = false)
    private Role role;

    @OneToMany(mappedBy = "courier")
    private List<CourierOrder> orderList;

    private String password;
    private boolean enabled;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;

    public User(String firstName, String lastName, String phoneNumber, District district, Date birthdate, UUID avatarId, Role role, String password, boolean enabled, LanguageEnum language) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.district = district;
        this.birthdate = birthdate;
        this.avatarId = avatarId;
        this.role = role;
        this.password = password;
        this.enabled = enabled;
        this.language = language;
    }

    public User(String firstName, String phoneNumber, Role role, String password, boolean enabled) {
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.password = password;
        this.enabled = enabled;
    }
}
