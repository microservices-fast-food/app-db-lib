package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.OrderHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderHistoryRepository extends JpaRepository<OrderHistory, UUID> {
    
}
