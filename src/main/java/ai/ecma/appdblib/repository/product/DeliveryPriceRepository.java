package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.DeliveryPrice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface DeliveryPriceRepository extends JpaRepository<DeliveryPrice, UUID> {



    Optional<DeliveryPrice> findByBranchId(UUID branch_id);
}
