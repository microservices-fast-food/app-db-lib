package ai.ecma.appdblib.repository.order;

import ai.ecma.appdblib.entity.order.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    List<Comment>getAllByOrderId(UUID orderId);
    Page<Comment> getAllByUnreadIsFalse(Pageable pageable);
    Page<Comment>getAllByUnreadIsTrue(Pageable pageable);
}
