package ai.ecma.appdblib.entity.product;

import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update delivery_price set deleted=true where id=?")
@Where(clause = "deleted=false")
public class DeliveryPrice extends AbsEntity {

    @Column(nullable = false)
    private Double minPrice; // minKm gacha bo'lgan masofa uchun bo'lgan pul

    @Column(nullable = false)
    private Double minKm;  // masalan - 4  km gacha minPrice bor

    @Column(nullable = false)
    private Double priceEveryKm; // minKm dan oshgan dan keyin har bir km uchun hisoblanadigan pul

    @OneToOne(fetch = FetchType.LAZY)
    private Branch branch;
}
