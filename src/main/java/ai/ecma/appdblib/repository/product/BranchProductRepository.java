package ai.ecma.appdblib.repository.product;

import ai.ecma.appdblib.entity.product.BranchProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BranchProductRepository extends JpaRepository<BranchProduct, UUID> {
    
}
