package ai.ecma.appdblib.repository.user;

import ai.ecma.appdblib.entity.user.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {


    
}
