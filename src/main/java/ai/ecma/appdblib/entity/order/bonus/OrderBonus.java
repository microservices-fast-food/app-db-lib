package ai.ecma.appdblib.entity.order.bonus;

import ai.ecma.appdblib.entity.order.Order;
import ai.ecma.appdblib.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity()
@SQLDelete(sql = "update order_bonus set deleted=true where id=?")
@Where(clause = "deleted=false")
public class OrderBonus extends AbsEntity {

    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Order order;

    @Column(name = "order_id", nullable = false)
    private UUID orderId;

    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private SaleBonus saleBonus;

    @Column(name = "sale_bonus_id")
    private UUID saleBonusId;

    @JoinColumn(name = "product_price_bonus_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductPriceBonus productPriceBonus;

    @Column(name = "product_price_bonus_id")
    private UUID productPriceBonusId;

    @JoinColumn(name = "product_count_bonus_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCountBonus productCountBonus;

    @Column(name = "product_count_bonus_id")
    private UUID productCountBonusId;

    @JoinColumn(name = "delivery_free_bonus_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private DeliveryFreeBonus deliveryFreeBonus;

    @Column(name = "delivery_free_bonus_id")
    private UUID deliveryFreeBonusId;

    @Column(nullable = false)
    private String boxName;

}
